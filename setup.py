from setuptools import setup
import os.path as osp

with open('VERSION', 'r') as f:
    version = f.read().strip()

with open('README.md', 'r') as f:
    long_description = f.read().strip()

setup(
       # the name is just a place holder that gives pip a specific name to store the
       # object under. Its only significant menaing is a handle to uninstall the plugin if necessary.
       name='highlight_merge',
       version=version,
       long_description=long_description,
       long_description_content_type="text/markdown",

       # The py_modules command tells pip which files need to be installed into the libraries location.
       # the ".py" is not included in the filename, and directories are separaet by "." instead of slashes.
       py_modules=['highlight_merge'],

       platforms=[
                    'linux',
                    'Unix'
                 ],

       # If this plugin requires any outside pip-installed components, they should be included here.
       # We specifically omit any for the sake of simplicity.
       # DO NOT include kwiver as e dependency if there is any chance the arrow will be used on a
       # source built kwiver. That would cause pip to install the kviwer wheel whcih will break the
       # source built kwiver install.
       install_requires=[
                        ],
       entry_points={
                'kwiver.python_plugin_registration':
                    [
                        # the name being assigned here doesnt really matter. The assignment side is the
                        # python sorurce file (same format as the py_modules above).
                        'highlight_merge=highlight_merge',
                    ],
                },
       classifiers=[
                        "Development Status :: 3 - Alpha",
                        "Intended Audience :: Developers",
                        "Programming Language :: Python :: 3.0",
                        "Programming Language :: Python :: 3.1",
                        "Programming Language :: Python :: 3.2",
                        "Programming Language :: Python :: 3.3",
                        "Programming Language :: Python :: 3.4",
                        "Programming Language :: Python :: 3.5",
                        "Programming Language :: Python :: 3.6",
                        "Programming Language :: Python :: 3.7",
                        "Programming Language :: Python :: 3.8",
                        "Operating System :: Unix",
                        "License :: OSI Approved :: BSD License"
                   ],
       python_requires=">=3.0",
    )

