from __future__ import print_function
from kwiver.vital.algo import MergeImages
from kwiver.vital import vital_logging
# the output of this arrow is an image which must be stored inside am
# implementation independent wrapper (ImageContainer)
from kwiver.vital.types import ImageContainer
from kwiver.vital.types import Image
import os
import sys
import cv2
import numpy


logger = vital_logging.getLogger(__name__)

# The ImageFilter class is the process that this arrow implements. It takes a single image
# in and produces a single image out per pass. Each pass is triggered by whatever source of
# images comes earlier in the pipeline.
class HighlightMerge(MergeImages):
    
    def __init__(self):
        MergeImages.__init__(self)

    def get_configuration(self):
        # Inherit from the base class
        cfg = super(MergeImages, self).get_configuration()
        return cfg

    # configuration options are established in the pipeline segment that instantiates this arrow.
    def set_configuration( self, cfg_in ):
        # load the colormap.
        cfg = self.get_configuration()
        cfg.merge_config(cfg_in)

        self.overlayamount = float(cfg.get_value("overlayfraction"))
        cmap = numpy.genfromtxt( [cfg.get_value("colormap")], delimiter=',', dtype=numpy.uint8)
        nmap = numpy.copy(cmap)
        nmap.resize(256 * 3 )
        self.colormap = nmap.reshape(256,1,3)

    def check_configuration( self, cfg):
        return True

    # The  MergeImages base class defines a merge function that must be implemented.
    def merge(self, base_image, segmentation):
        # apply the segmentation image to the base_image using self.colormap and transparency
        bimage = base_image.image().asarray()
        print( bimage.shape )
        mat = segmentation.image().asarray()
        print( mat.shape )
        mat2 = cv2.resize( mat, (bimage.shape[1], bimage.shape[0])  )
        print( mat2.shape )
        img = cv2.merge((mat2,mat2,mat2))
       
        overlay = cv2.LUT(img, self.colormap)
        out_image = cv2.addWeighted(bimage,self.overlayamount, overlay, 1-self.overlayamount,0 )
        
        return ImageContainer(Image(out_image))

# The __vital_algorithm_register__ establishes an entry point that kwiver and plugin_explorer use to
# identify usable plugins.
def __vital_algorithm_register__():
    from kwiver.vital.algo import algorithm_factory
    # Register Algorithm
    # the name indicates the name that is used to instantiate this type of image_filter.
    implementation_name  = "HighlightMerge"
    # check if the type is already instantiated somewhere else.
    if algorithm_factory.has_algorithm_impl_name( HighlightMerge.static_type_name(), implementation_name):
        return
    algorithm_factory.add_algorithm( implementation_name, "Merge the second image into the first by coloring it.", HighlightMerge )
    algorithm_factory.mark_algorithm_as_loaded( implementation_name )
